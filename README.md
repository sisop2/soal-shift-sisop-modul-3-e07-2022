# soal-shift-sisop-modul-3-E07-2022
### Anggota Kelompok E07
|Nama|NRP|
|--------------|--------------|
|Tegar Ganang Satrio Priambodo|5025201002|
|Rafael Asi Kristanto Tambunan|5025201168|
|Ahmad Ibnu Malik Rahman|5025201232|

## Soal 1
1a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread
```
int main()
{
    pthread_t thread1, thread2, thread3;
    char *file_name1 = "music";
    char *file_name2 = "quote";
    
    // a-d
    pthread_create(&thread1, NULL, downloadUnzip, (char*) file_name1);
    pthread_create(&thread2, NULL, downloadUnzip, (char*) file_name2);
    pthread_create(&thread3, NULL, zipHasil, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL); 
    pthread_join(thread3, NULL); 
```
1b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
```
void* downloadUnzip(char* folder_name)
{
    pid_t ch1, ch2, ch3, ch4, ch5;
    ch1 = fork();
    int status1;

    if (ch1 == 0) {
        ch2 = fork();
        int status2;

        if (ch2 == 0) {
            if (strcmp(folder_name, "music") == 0) {
                char *argv[] = {"wget", "--no-check-certificate", src[0], "-O", "music.zip", NULL};
                execv("/bin/wget", argv);
            } 
            else if (strcmp(folder_name, "quote") == 0) {
                char *argv[] = {"wget", "--no-check-certificate", src[1], "-O", "quote.zip", NULL};
                execv("/bin/wget", argv);
            }
        }
        else {
            while ((wait(&status2)) > 0);
            char *argv[] = {"unzip", folder_name, "-d", folder_name, NULL};
            execv("/bin/unzip", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        ch3 = fork();
        int status3;

        if (ch3 == 0) {
            ch4 = fork();
            int status4;
            if (ch4 == 0) { // remove zip file
                char *file_name = malloc(sizeof(char) * 100);
                strcpy(file_name, folder_name);
                strcat(file_name, ".zip");
                char *argv[] = {"rm", file_name};
                execv("/bin/rm", argv);
            } 
            else { // create .txt file
                while((wait(&status4)) > 0);
                char *file_name = malloc(sizeof(char) * 100);
                strcpy(file_name, folder_name);
                strcat(file_name, ".txt");
                char *argv[] = {"touch", file_name, NULL};
                execv("/bin/touch", argv);
            }
        } 
        else {
            while ((wait(&status3)) > 0);
            ch5 = fork();
            int status5;

            if (ch5 == 0) { // listing directory
                DIR *dir_path;
                struct dirent *dir;
                dir_path = opendir(folder_name);
                while((dir = readdir(dir_path))) {
                    if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
                        readLine(folder_name, dir->d_name);
                }
            } 
            else {
                while ((wait(&status5)) > 0);
                moveFolder(folder_name, folder_name); // move .txt files to folder hasil
            }
        }
    }
}

void* readLine(char* folder_name, char* file_name)
{
    char filepath[100]; 
    strcpy(filepath, folder_name);
    strcat(filepath, "/");
    strcat(filepath, file_name);

    char file_txt[100];
    strcpy(file_txt, folder_name);
    strcat(file_txt, ".txt");

    FILE *file;
    file = fopen(filepath, "a+");

    FILE *file_base64;
    file_base64 = fopen(file_txt, "a");

    char str[255];
    while(fgets(str, 255, file)) {
        printf("%s\n", str);
        fprintf(file_base64, "%s\n", decode(str));
    }

    fclose(file);
}

char* decode(char* str)
{
    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(str) * 3 / 4 + 1);
    int i = 0, p = 0;

    for (i = 0; str[i] != '\0'; i++) {
        char k;
        for (k = 0 ; k < 64 && base64[k] != str[i]; k++);
        buffer[counts++] = k;
        if (counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    plain[p] = '\0';
    return plain;
}
```
1c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil
```
void* moveFolder(char* folder_name, char* file_name)
{
    pid_t ch1, ch2;

    ch1 = fork();
    int status1;
    if (ch1 == 0) {
        ch2 = fork();
        int status2;

        if (ch2 == 0) { // remove folder after decoded all .txt files
            char *argv[] = {"rm", "-rf", folder_name, NULL};
            execv("/bin/rm", argv);
        } 
        else { // create directory hasil
            while ((wait(&status2)) > 0);
            char *argv[] = {"mkdir", "-p", "hasil", NULL};
            execv("/bin/mkdir", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        pid_t child_id_3 = fork();
        int status3;

        if (child_id_3 == 0) { // move .txt into hasil
            char filename[100];
            strcpy(filename, folder_name);
            strcat(filename, ".txt");
            char *argv[] = {"mv", filename, "hasil", NULL};
            execv("/bin/mv", argv);
        } 
        else {
            while ((wait(&status3)) > 0);
            status += 1;
        }
    }
}
```
1d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
```
void* zipHasil()
{
    pid_t ch1, ch2;
    int status1;

    while (status != 2) {
    }
  
    ch1 = fork();
    if (ch1 == 0) {
        int status2;
        ch2 = fork();

        if (ch2 == 0) { // zip folder hasil
            char* argv[] = {"zip", "-r", "-P", "mihinomenestibnumalik", "hasil.zip", "hasil", NULL};
            execv("/bin/zip", argv);
        } 
        else { // remove folder hasil
            while ((wait(&status2)) > 0);
            char *argv[] = {"rm", "-rf", "hasil", NULL};
            execv("/bin/rm", argv);
        }
    } 
    else
        while ((wait(&status1)) > 0);
}
```
1e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```
// e
    pthread_create(&thread1, NULL, unzipWithPassword, "hasil.zip");
    pthread_create(&thread2, NULL, noTxt, NULL);
    pthread_create(&thread3, NULL, zipHasil, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);

    exit(EXIT_SUCCESS);
}
```
```
void* unzipWithPassword()
{
    pid_t ch1;
    int status1;

    ch1 = fork();
    if (ch1 == 0) {
        pid_t ch2 = fork();
        int status2;
        if (ch2 == 0) { // unzip hasil.zip with password
            char* argv[] = {"unzip", "-P", "mihinomenestibnumalik", "hasil.zip", NULL};
            execv("/bin/unzip", argv);
        } 
        else { // remove hasil.zip
            while ((wait(&status2)) > 0);
            char *argv[] = {"rm", "hasil.zip", NULL};
            execv("/bin/rm", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        status = 3;
    }
}

void* noTxt()
{
    pid_t ch1;
    int status1;

    ch1 = fork();
    if (ch1 == 0) {
        pid_t ch2 = fork();
        int status2;
        if (ch2 == 0) { // create no.txt
            char* argv[] = {"touch", "no.txt", NULL};
            execv("/bin/touch", argv);
        } 
        else { // write "No" and move to folder hasil/
            while ((wait(&status2)) > 0);
            FILE *file_no_txt;
            file_no_txt = fopen("no.txt", "w+");
            fprintf(file_no_txt, "No");
            fclose(file_no_txt);
            char *argv[] = {"mv", "no.txt", "hasil", NULL};
            execv("/bin/mv", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        status = 2;
    }
}
```
Dokumentasi
![image](/uploads/882da032a2d417410d7908cc95014913/image.png)
![image](/uploads/66a5934757fd2d4195f93153f0aec9c2/image.png)
![image](/uploads/59372680c6b24bbce82c4dcb99c16164/image.png)

## Soal 2
Terdapat Variabel-varibael yang diperlukan, lalu ada scanf dari input untuk menentukan apakah register atau login
![image](/uploads/b873bb682f6b5bdddc454c94b640fafe/image.png)

Jika input berupa register, maka buat username terlebih dahulu dengan menginputnya dengan variabel input lalu akan dibaca dengan buffer oleh valread yang dimana buffer akan dibandingkan jika ada username yang sudah ada maka akan dioutputkan username tersebut dan akan dibuat username baru jika usernamenya belum ada maka printf buffer dari username tersebut
![image](/uploads/d930204a11d9193fe1b5c6ea3d1783f3/image.png)

Jika Username sudah dibuat lalu akan dibuat Password dengan ketentuan harus memiliki uppercase, lowercase, dan angka. Lalu diinput Password yang dibuat dengan ketentuan harus lebih dari 6 karakter. Jika tidak lebih maka akan dibuat ulang. Lalu dicek dari Password apakah memiliki dari uppercase, lowercase, dan angka. Maka dibuat variabel yang akan membandingkan Password yang diinput nantinya dengan mengecek tiap indeks dari password dengan perbandingan pada ASSCI Code yang sesuai. Jika  terdapat salah satu dari ketiga ketentuan tersebut tidak terpenuhi maka variabel akan tetap bernilai 0, yang dimana akan membuat password baru. Jika Ketentuan sudah benar semua maka aka diprint buffer.
![image](/uploads/54222407e03e6d296fcf2b3e9694237b/image.png)

Jika isi input yang pertama adalah Login, maka akan dimasukkan ke fungsi login. Yang pertama adalah login dari username, dengan membandingkan buffer jika username tidak ada maka akan dikembalikan ke login username untuk login kembali
![image](/uploads/778576183a30c4b97a53752f55257981/image.png)

Jika sudah menginput dari login username maka akan diinput lagi password dari username tersebut. Dicek kembali apakah password dari username itu sama jika tidak maka diberitahukan bahwa password itu salah lalu diulang kembali untuk memasukkan password dari username tersebut
![image](/uploads/96749c2ac98aaef2d43068524c21b8b7/image.png)

## Soal 3
3a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

Membuat fungsi extract file
```
void extract(char *value) {
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```

Ketika dijalankan akan ada perintah unzip pathfile. Kemudian untuk melakukan pengategorian file codenya sebagai berikut
```
void sort_file(char *from){
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/tegar/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }

    if(!flag){
        printf(":(");
    }else {
        printf(":(\n");
    }
}
```
program membaca apa saja dalam directory hingga bernilai NULL, lalu thread akan menjalangkan fungsi move_move untuk memindahkan file menuju folder sesuai dengan format yang dituju
```
void *move_move(void *s_path){
    struct_path s_paths = *(struct_path*) s_path;
    move_file(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}

void move_file(char *p_from, char *p_cwd){
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    convert_to_lower(file_ext);

    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    create_dir(file_ext);
    rename(p_from, p_cwd);
}

```

3b Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

```
while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

```

3c Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

```
while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move_move, (void *)&s_path);
            sleep(1);
            index++;
        }

```
terdapat while loop pada fungsi sort_file. setiap iterasi dibuatkan 1 therad untuk memindahkan file

3d 

Dokumentasi

Terdapat error saat mengcompile client.c

![image](/uploads/968eb7faf034b36f629c463eb394c3fd/image.png)
