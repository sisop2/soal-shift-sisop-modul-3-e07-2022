#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>

int status = 0;
char *src[] = {"https://docs.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", 
            "https://docs.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt"};
char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void* downloadUnzip(char* folder_name);
void* readLine(char* folder_name, char* file_name);
char* decode(char* str);
void* moveFolder(char* folder_name, char* file_name);
void* zipHasil();
void* unzipWithPassword();
void* noTxt();

void* downloadUnzip(char* folder_name)
{
    pid_t ch1, ch2, ch3, ch4, ch5;
    ch1 = fork();
    int status1;

    if (ch1 == 0) {
        ch2 = fork();
        int status2;

        if (ch2 == 0) {
            if (strcmp(folder_name, "music") == 0) {
                char *argv[] = {"wget", "--no-check-certificate", src[0], "-O", "music.zip", NULL};
                execv("/bin/wget", argv);
            } 
            else if (strcmp(folder_name, "quote") == 0) {
                char *argv[] = {"wget", "--no-check-certificate", src[1], "-O", "quote.zip", NULL};
                execv("/bin/wget", argv);
            }
        }
        else {
            while ((wait(&status2)) > 0);
            char *argv[] = {"unzip", folder_name, "-d", folder_name, NULL};
            execv("/bin/unzip", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        ch3 = fork();
        int status3;

        if (ch3 == 0) {
            ch4 = fork();
            int status4;
            if (ch4 == 0) { // remove zip file
                char *file_name = malloc(sizeof(char) * 100);
                strcpy(file_name, folder_name);
                strcat(file_name, ".zip");
                char *argv[] = {"rm", file_name};
                execv("/bin/rm", argv);
            } 
            else { // create .txt file
                while((wait(&status4)) > 0);
                char *file_name = malloc(sizeof(char) * 100);
                strcpy(file_name, folder_name);
                strcat(file_name, ".txt");
                char *argv[] = {"touch", file_name, NULL};
                execv("/bin/touch", argv);
            }
        } 
        else {
            while ((wait(&status3)) > 0);
            ch5 = fork();
            int status5;

            if (ch5 == 0) { // listing directory
                DIR *dir_path;
                struct dirent *dir;
                dir_path = opendir(folder_name);
                while((dir = readdir(dir_path))) {
                    if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
                        readLine(folder_name, dir->d_name);
                }
            } 
            else {
                while ((wait(&status5)) > 0);
                moveFolder(folder_name, folder_name); // move .txt files to folder hasil
            }
        }
    }
}

void* readLine(char* folder_name, char* file_name)
{
    char filepath[100]; 
    strcpy(filepath, folder_name);
    strcat(filepath, "/");
    strcat(filepath, file_name);

    char file_txt[100];
    strcpy(file_txt, folder_name);
    strcat(file_txt, ".txt");

    FILE *file;
    file = fopen(filepath, "a+");

    FILE *file_base64;
    file_base64 = fopen(file_txt, "a");

    char str[255];
    while(fgets(str, 255, file)) {
        printf("%s\n", str);
        fprintf(file_base64, "%s\n", decode(str));
    }

    fclose(file);
}

char* decode(char* str)
{
    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(str) * 3 / 4 + 1);
    int i = 0, p = 0;

    for (i = 0; str[i] != '\0'; i++) {
        char k;
        for (k = 0 ; k < 64 && base64[k] != str[i]; k++);
        buffer[counts++] = k;
        if (counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    plain[p] = '\0';
    return plain;
}

void* moveFolder(char* folder_name, char* file_name)
{
    pid_t ch1, ch2;

    ch1 = fork();
    int status1;
    if (ch1 == 0) {
        ch2 = fork();
        int status2;

        if (ch2 == 0) { // remove folder after decoded all .txt files
            char *argv[] = {"rm", "-rf", folder_name, NULL};
            execv("/bin/rm", argv);
        } 
        else { // create directory hasil
            while ((wait(&status2)) > 0);
            char *argv[] = {"mkdir", "-p", "hasil", NULL};
            execv("/bin/mkdir", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        pid_t child_id_3 = fork();
        int status3;

        if (child_id_3 == 0) { // move .txt into hasil
            char filename[100];
            strcpy(filename, folder_name);
            strcat(filename, ".txt");
            char *argv[] = {"mv", filename, "hasil", NULL};
            execv("/bin/mv", argv);
        } 
        else {
            while ((wait(&status3)) > 0);
            status += 1;
        }
    }
}

void* zipHasil()
{
    pid_t ch1, ch2;
    int status1;

    while (status != 2) {
    }
  
    ch1 = fork();
    if (ch1 == 0) {
        int status2;
        ch2 = fork();

        if (ch2 == 0) { // zip folder hasil
            char* argv[] = {"zip", "-r", "-P", "mihinomenestibnumalik", "hasil.zip", "hasil", NULL};
            execv("/bin/zip", argv);
        } 
        else { // remove folder hasil
            while ((wait(&status2)) > 0);
            char *argv[] = {"rm", "-rf", "hasil", NULL};
            execv("/bin/rm", argv);
        }
    } 
    else
        while ((wait(&status1)) > 0);
}

void* unzipWithPassword()
{
    pid_t ch1;
    int status1;

    ch1 = fork();
    if (ch1 == 0) {
        pid_t ch2 = fork();
        int status2;
        if (ch2 == 0) { // unzip hasil.zip with password
            char* argv[] = {"unzip", "-P", "mihinomenestibnumalik", "hasil.zip", NULL};
            execv("/bin/unzip", argv);
        } 
        else { // remove hasil.zip
            while ((wait(&status2)) > 0);
            char *argv[] = {"rm", "hasil.zip", NULL};
            execv("/bin/rm", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        status = 3;
    }
}

void* noTxt()
{
    pid_t ch1;
    int status1;

    ch1 = fork();
    if (ch1 == 0) {
        pid_t ch2 = fork();
        int status2;
        if (ch2 == 0) { // create no.txt
            char* argv[] = {"touch", "no.txt", NULL};
            execv("/bin/touch", argv);
        } 
        else { // write "No" and move to folder hasil/
            while ((wait(&status2)) > 0);
            FILE *file_no_txt;
            file_no_txt = fopen("no.txt", "w+");
            fprintf(file_no_txt, "No");
            fclose(file_no_txt);
            char *argv[] = {"mv", "no.txt", "hasil", NULL};
            execv("/bin/mv", argv);
        }
    } 
    else {
        while ((wait(&status1)) > 0);
        status = 2;
    }
}

int main()
{
    pthread_t thread1, thread2, thread3;
    char *file_name1 = "music";
    char *file_name2 = "quote";
    
    // a-d
    pthread_create(&thread1, NULL, downloadUnzip, (char*) file_name1);
    pthread_create(&thread2, NULL, downloadUnzip, (char*) file_name2);
    pthread_create(&thread3, NULL, zipHasil, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL); 
    pthread_join(thread3, NULL); 

    status = 0;

    // e
    pthread_create(&thread1, NULL, unzipWithPassword, "hasil.zip");
    pthread_create(&thread2, NULL, noTxt, NULL);
    pthread_create(&thread3, NULL, zipHasil, NULL);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);

    exit(EXIT_SUCCESS);
}