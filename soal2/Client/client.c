#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#define PORT 8080

int main(int argc, char const *argv[]) {
    char input[50];
    int sock = 0, valread;
    char buffer[1000] = {0};

        scanf("%s", input);

        if (strcmp(input, "register") == 0){

            username:
                memset(buffer, 0, 1000);
                printf("Username: ");
                scanf("%s", input);
     

                valread = read(sock, buffer, 1000);
                if (strcmp(buffer, "Username sudah ada") == 0){
                    printf("%s\n", buffer);
                    goto username;
                }else
                    printf("%s\n", buffer);

            int uppercase, lowercase, number;
            password:
                memset(buffer, 0, 1000);
                uppercase = 0;
                lowercase = 0;
                number = 0;
                printf("Password: ");
                scanf("%s", input);
                if (strlen(input) < 6){
                    printf("\nPassword minimal terdiri dari 6 karakter\n\n");
                    goto password;
                }
                for (int i = 0; i<strlen(input); i++){
                    if (input[i]>=48 && input[i]<=57)
                        number = 1;
                    if (input[i]>=65 && input[i]<=90)
                        uppercase = 1;
                    if (input[i]>=97 && input[i]<=122)
                        lowercase = 1;
                }
                if (number == 0 || uppercase == 0 || lowercase == 0){
                    printf("\nPassword minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil\n\n");
                    goto password;
                }
                printf("Akun terdaftar\n");
        }

        else if (strcmp(input, "login") == 0){
            
            login_username:
                memset(buffer, 0, 1000);
                printf("Username: ");
                scanf("%s", input);

                valread = read(sock, buffer, 1000);
                if (strcmp(buffer, "Username tidak ada") == 0){
                    printf("%s\n", buffer);
                    goto login_username;
                }

            login_password:
                memset(buffer, 0, 1000);
                printf("Password: ");
                scanf("%s", input);

                valread = read(sock, buffer, 1000);
                if (strcmp(buffer, "Password Salah") == 0){
                    printf("%s\n", buffer);
                    goto login_password;
                }else
                    printf("%s\n", buffer);
        }
}